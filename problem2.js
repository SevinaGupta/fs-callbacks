const fs = require('fs');

//1. Read the given file lipsum.txt
function fileRead(file,cb){

	fs.readFile(file, function (err, data) {
		if (err) {
		   return console.error(err);
		}
		//console.log(data.toString());
		console.log("read file")
		cb();
	 });

}



// 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt

let filenames=''
function writeFileInUpperCase(file,cb){
 fs.readFile(file, function (err, data) {
	if (err) {
	   return console.error(err);
	}
	let dataInUpperCase = data.toString().toUpperCase();
	//console.log(upperCase)
	fs.writeFile('../dataUpperCase.txt', dataInUpperCase ,()=>{
		
		console.log("Upper Case Data written successfully!");

		filenames = '../dataUpperCase.txt\n'
		fs.appendFile("filenames.txt",filenames,()=>{
			console.log("append Successfully")

		})
		cb();
	
		 })
})
}


// // 3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt

function writeSplitDataInLowerCase(file,cb){
 fs.readFile(file, function (err, data) {
	if (err) {
	   return console.error(err);
	}
	let lowerCase = data.toString().toLowerCase();
	let result = lowerCase.split(".").join("\n")
    
	fs.writeFile('../splitDataInLowerCase.txt',result,()=>{
		console.log("split Data written successfully!");

		filenames = "../splitDataInLowerCase.txt\n"
		fs.appendFile("filenames.txt",filenames,()=>{
			 console.log("append Successfully")
		cb()
		})
	})

 });

}



// // 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt

function writeSortDataInFile(file,cb){
	fs.readFile(file, function (err, data) {
	   if (err) {
		  return console.error(err);
	   }
	   
	   function result (str){
	   let arr = str.split("\n")
	   let sortArr = arr.sort()
	return sortArr.join("\n");
	
}
	fs.writeFile('../sortData.txt',result(data.toString()),()=>{
		console.log("sort Data written successfully!");

		filenames = "../sortData.txt\n"
		fs.appendFile("filenames.txt",filenames,()=>{
		  console.log("append Successfully")
		
	  cb()
		})
	})
 });

}


// // 5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.

function deleteAllFiles(file){
	fs.readFile(file, function (err, data) {
		if (err) {
		   return console.error(err);
		}
	
	
	let fileName = function(str){
		return str.trim().split("\n")
	}
  
   let fileNameArray = fileName(data.toString());
	
		fileNameArray.forEach((filepath)=>{
			fs.unlink(filepath,function(err){
				if (err) console.log(err)
				console.log("all files are delete")
			})
		 })
		});
}

module.exports = {fileRead,writeFileInUpperCase,writeSplitDataInLowerCase,writeSortDataInFile,deleteAllFiles} 